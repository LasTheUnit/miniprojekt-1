package dreamteam.miniprojekt1;

/**
 *
 * @author Las Osman
 * @author Julia Boberg
 * @email lasosman@outlook.com
 * @version 27 jan. 2022
 */
public class Counter {
    private int count;

    public Counter() {
        count = 0;
    }
    
    public void tick(){count++;}
    
    public void reset(){count = 0;}
    
    public int getCount(){return count;}
    
    
}
