/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dreamteam.miniprojekt1;

import java.awt.event.ActionEvent;
import javax.swing.Timer;


/**
 *
 * @author Las Osman
 * @author Julia Boberg
 * @email lasosman@outlook.com
 * @version 27 jan. 2022
 */
public class Clock {
    private Counter counter;
    private Timer timer;

    public Clock() {
        counter = new Counter();
        timer = new Timer(1000, (ActionEvent e) -> {
           counter.tick();
        });
    }
    
    public int getSeconds() {
        return counter.getCount()%60;
    }
    
    public int getMinutes() {
        return (counter.getCount()/60)%60;
    }
    
    public int getHours() {
        return (counter.getCount()/60/60)%60;
    }
    
    public void reset() {
        counter.reset();
    }
    
    public void start() {
        timer.start();
    }
    
    public void stop() {
        timer.stop();
    }

    @Override
    public String toString() {
        return String.format("%d:%d:%d", this.getHours(), this.getMinutes(), this.getSeconds());
    }
    
    
}
