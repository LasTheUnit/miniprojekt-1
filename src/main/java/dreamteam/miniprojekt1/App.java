

package dreamteam.miniprojekt1;

/**
 *
 * @author Las Osman
 * @author Julia Boberg
 * @email lasosman@outlook.com
 * @version 27 jan. 2022
 */
public class App {
    public static void main(String[] args) {
        ChessClock chessClock = new ChessClock();
        
        chessClock.startPlayerOneClock();
        try {
            Thread.sleep(15500);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        System.out.println(chessClock.toString());
        
        chessClock.startPlayerTwoClock();
        
         try {
            Thread.sleep(4500);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        System.out.println(chessClock.toString());
        
        chessClock.reset();
        
        System.out.println(chessClock.toString());
        
    }
}
