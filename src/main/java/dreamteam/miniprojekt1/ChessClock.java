/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dreamteam.miniprojekt1;

/**
 *
 * @author Las Osman
 * @author Julia Boberg
 * @email lasosman@outlook.com
 * @version 27 jan. 2022
 */
public class ChessClock {
    
    enum Turn {
        PLAYER_ONE,
        PLAYER_TWO,
        NONE
    }
    
    private Clock playerOneClock;
    private Clock playerTwoClock;
    private Turn turn;

    public ChessClock() {
        playerOneClock = new Clock();
        playerTwoClock = new Clock();
        turn = Turn.NONE;
    }
    
    public void startPlayerOneClock(){
        if(turn != Turn.PLAYER_ONE){
            playerOneClock.start();
            playerTwoClock.stop();
            turn = Turn.PLAYER_ONE;
        }
    }
    
    public void startPlayerTwoClock(){
        if(turn != Turn.PLAYER_TWO){
            playerOneClock.stop();
            playerTwoClock.start();
            turn = Turn.PLAYER_TWO;
        }
    }
    
    public void reset(){
        playerOneClock.reset();
        playerTwoClock.reset();
        turn = Turn.NONE;
    }
    
    @Override
    public String toString() {
        return " Player One: " + playerOneClock.toString() + " Player Two: " + playerTwoClock.toString();
    }
}
